#include <cmath>

#include "entropycalculator.h"

EntropyCalculator::EntropyCalculator()
    :_totalCount(0) {}

double EntropyCalculator::entropy() const {
    double H = 0;
    for (const auto &i : _counts) {
        auto prob = (double)i.second / _totalCount;
        H -= prob * log2(prob);
    }
    puts("");

    return H;
}

void EntropyCalculator::addSymbol(char c) {
    if (_counts.find(c) != _counts.end()) {
        _counts.insert(std::make_pair(c, 0));
    }
    _counts[c] += 1;
    ++_totalCount;
}

void EntropyCalculator::clear() {
    _counts.clear();
    _totalCount = 0;
}
