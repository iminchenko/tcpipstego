#pragma once

#include <map>

class EntropyCalculator {
public:
    EntropyCalculator();

    // вычислить энтропию
    double entropy() const;

    // добавить символ
    void addSymbol(char c);

    void clear();

private:
    std::map<char, int> _counts;
    int _totalCount;
};
