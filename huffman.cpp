#include <iostream>
#include <vector>
#include <list>
#include <cassert>
#include <math.h>
#include <memory>
#include <limits>
#include <set>
#include <utility>
#include <algorithm>

#include "huffman.h"

using std::vector;
using std::list;
using std::shared_ptr;
using std::set;
using std::pair;
using std::string;

using byte = char;
using block_t = uint8_t;
using bits = vector<bool>;

constexpr size_t STARTING_VECTOR_SIZE = 8;
constexpr size_t BYTE_SIZE = sizeof(byte) * 8;
constexpr size_t BYTE_SIZE_1 = BYTE_SIZE - 1;
// допустим, что кодировать блоки длиннее байта неэфективно
constexpr int MAX_BLOCKLEN = 8;
// кол-во бит, необходимое для храниения длины дополнения последовательости
constexpr int PAD_COUNT_LEN = 4;

// максимальное количество циклов кодирования
constexpr int MAX_CYCLE = 15;
// кол-во бит, необходимое для храниения длины количества циклов
constexpr int CYCLE_COUNT_LEN = 4;

constexpr bool NOT_LEAF_BIT = 1;
constexpr bool LEAF_BIT = 0;

constexpr bool LEFT_BIT = 0;
constexpr bool RIGHT_BIT = 1;

bits encodeHuffman(const bits &original, int blocklen);
bits decodeHuffman(const bits &encoded);

void padSequence(bits &sequence, int blockLength) {
    int padCountLength = ceil(log2(blockLength) + 1);
    // необходимо дополнить последовательность так, чтобы она была кратна
    // длине блока
    int mod = (sequence.size() + padCountLength) % blockLength;
    int diff = blockLength - mod;
    sequence.resize(sequence.size() + diff + padCountLength);
    // дописывание количества дополненных символов
    for (int i = 0; i < padCountLength; ++i)
        sequence[sequence.size() - 1 - i] = (diff >> i) & 1;
}

void unpadSequence(bits &sequence, int blockLength) {
    if (!sequence.size())
        return;

    int padCountLength = ceil(log2(blockLength) + 1);
    int diff = 0;
    for (int i = 0; i < padCountLength; ++i) {
        if (sequence[sequence.size() - i - 1])
            diff |= 1 << i;
    }

    sequence.resize(sequence.size() - padCountLength - diff);
}

vector<int> blocksCount(const bits &sequence, int blockLength) {
    assert(!(sequence.size() % blockLength));

    vector<int> counts(1 << blockLength, 0);

    block_t block = 0;
    int counter = 0;

    for (auto bit : sequence) {
        block <<= 1;
        block |= bit ? 1 : 0;
        if (++counter == blockLength) {
            ++counts[block];
            block = 0;
            counter = 0;
        }
    }

    return std::move(counts);
}

double calculateEntropy(const bits &sequence, int blockLenght) {
    assert(!(sequence.size() % blockLenght));

    int summaryCount = sequence.size() / blockLenght;

    double entropy = 0;

    auto counts = blocksCount(sequence, blockLenght);

    for (const auto &i : counts) {
        if (i) {
            double p = static_cast<double>(i) / summaryCount;
            entropy -= p * log2(p);
        }
    }

    return entropy;
}

int optimalBlockLength(const bits &sequence) {
    int bestLength = 0;
    double record = std::numeric_limits<double>::max();

    for (int i = 2; i <= MAX_BLOCKLEN; ++i) {
        bits encoded = encodeHuffman(sequence, i);

        if (encoded.size() < record) {
            bestLength = i;
            record = encoded.size();
        }
    }

    return bestLength;
}

// есть ли в кнтейнере хотя бы 2 элемента
template <typename T>
bool hasTwoItems(const T& l) {
    auto iter = l.cbegin();

    if (iter == l.cend())
        return false;
    if (++iter == l.cend())
        return false;

    return true;
}

bits bytesToBits(vector<byte> &vec) {
    if (!vec.size())
        return bits();

    bits seq(vec.size() * 8);

    int i = 0;

    for (auto b : vec) {
        for (int j = BYTE_SIZE_1; j >= 0; --j) {
            seq[i++] = (b >> j) & 1;
        }
    }

    return std::move(seq);
}

vector<byte> bitsToBytes(const bits &seq) {
    if (!seq.size())
        return vector<byte>();

    assert(!(seq.size() % 8));

    vector<byte> bytes(seq.size() / 8);

    int i = 0;

    for (auto &b : bytes) {
        b = 0;
        for (int j = BYTE_SIZE_1; j >= 0; --j) {
            if (seq[i++])
                b |= 1 << j;
        }
    }

    return std::move(bytes);
}

struct node;

using node_ptr = shared_ptr<node>;

template <class... Types>
node_ptr make_node_ptr(Types &...arr) {
    return std::make_shared<node>(arr...);

}

//using node_ptr = node*;

//template <class... Args>
//node_ptr make_node_ptr(Args ...args) {
//    return new node(std::forward<Args>(args)...);
//}

struct node {
    block_t value;
    int count;
    const bool leaf;

    node_ptr left;
    node_ptr right;

    node(block_t _value, int _count = 0)
        :value(_value), count(_count), leaf(true) {}
    node(node_ptr _left, node_ptr _right)
        :value(0), count(_left->count + _right->count), leaf(false),
         left(_left), right(_right) {}
    node() :value(0), count(0), leaf(false) {}
};

using codePair = pair<block_t, bits>;

struct comp {
bool operator() (const codePair &p1, const codePair &p2) { return p1.first < p2.first; }
};

class HuffmanTree {
public:
    HuffmanTree (): _blockLength(0), _updated(true) {}
    HuffmanTree(vector<int> counts, int blockLength) {
        buildTree(counts, blockLength);
    }

    int getBlockLength() const {
        return _blockLength;
    }

    set<pair<block_t, bits>, comp> getCodes() const {
        calculateCodes();

        return _codes;
    }

    bool containSequence(const bits &sequence) const {
        if (_root->leaf)
            return sequence == bits{0};

        int i = 0;

        node_ptr cur = _root;

        while (i < sequence.size()) {
            if (cur->leaf)
                return false;

            cur = (sequence[i++] == LEFT_BIT) ? cur->left : cur->right;
        }

        if (!cur->leaf)
            return false;

        return true;
    }

    block_t decode(const bits &sequence) {
        if (_root->leaf) {
            assert(sequence == bits{ 0 });
            return _root->value;
        }

        int i = 0;

        node_ptr cur = _root;

        while (i < sequence.size()) {
            assert(!cur->leaf);

            cur = (sequence[i++] == LEFT_BIT) ? cur->left : cur->right;
        }

        assert(cur->leaf);

        return cur->value;
    }

    bits encode(block_t block) const {
        calculateCodes();

//        auto iter = std::find_if(_codes.cbegin(), _codes.cend(),
//                [&](auto &p){ return p.first == block; });
        auto iter = _codes.find(pair<block_t, bits>(block, bits()));

        assert(iter != _codes.cend());

//        bits code = findBlockCode(_root, block, bits());

//        assert(code.size());

        return iter->second;
    }

    static bits findBlockCode(node_ptr tree, block_t block, bits curPrefix) {
        if (tree->leaf) {
            if (tree->value == block)
                return curPrefix;
        }
        else {
            bits temp = curPrefix;
            temp.push_back(LEFT_BIT);
            bits res = findBlockCode(tree->left, block, temp);
            if (res.size())
                return res;

            res.back() = RIGHT_BIT;
            res = findBlockCode(tree->right, block, temp);
            if (res.size())
                return res;
        }

        return bits();
    }

    void buildTree(vector<int> counts, int blockLength) {
        _updated = true;

        _blockLength = blockLength;
        list<node_ptr> trees;

        // лень заморачиваться с очередью с приоритетами ¯\_(ツ)_/¯
        auto addRoot = [&](node_ptr nptr) {
            auto iter = trees.begin();

            while (iter != trees.cend() && (*iter)->count < nptr->count) {
                ++iter;
            }

            trees.insert(iter, nptr);
        };

        for (int i = 0; i < counts.size(); ++i) {
            if (counts[i]) {
                addRoot(make_node_ptr(i, counts[i]));
            }
        }

        // пока не останется один корень
        while (hasTwoItems(trees)) {
            auto iter1 = trees.begin();
            auto iter2 = trees.begin();
            ++iter2;

            node_ptr p1 = *iter1;
            node_ptr p2 = *iter2;

            trees.erase(iter1);
            trees.erase(iter2);

            addRoot(make_node_ptr(p1, p2));
        }

        _root = trees.front();
    }

    bits serealize() const {
        auto lst = serealizeTreeImpl(_root);

        // pad count len на информацию о длине блока
        bits prefix(lst.size() + PAD_COUNT_LEN);

        int i = 0;
        for (; i < PAD_COUNT_LEN; ++i)
            prefix[i] = _blockLength & (1 << (PAD_COUNT_LEN - i - 1));

        for (auto bit : lst) {
            prefix[i++] = bit;
        }

        return prefix;
    }

    // возвращает символ номер символа последовательности
    // на котором закончилась загрузка
    int load(const bits &serealized) {
        _updated = true;
        int i = 0;
        _blockLength = 0;

        for (; i < PAD_COUNT_LEN && i < serealized.size(); ++i) {
            _blockLength <<= 1;
            _blockLength |= serealized[i] ? 1 : 0;
        }

        loadTreeImpl(serealized, i, _root);

        return i;
    }

private:
    list<bool> serealizeTreeImpl(node_ptr n) const {
        list<bool> lst;

        if (n->leaf) {
            lst.push_back(LEAF_BIT);
            for (int i = _blockLength - 1; i >= 0; --i) {
                lst.push_back(n->value & (1 << i));
            }
        }
        else {
            lst.push_back(NOT_LEAF_BIT);
            lst.splice(lst.end(), serealizeTreeImpl(n->left));
            lst.splice(lst.end(), serealizeTreeImpl(n->right));
        }

        return std::move(lst);
    }

    void loadTreeImpl(const bits &serealized, int &i, node_ptr &root) {
        if (i >= serealized.size()) {
            throw "Can't load tree";
        }
        if (serealized[i++] == NOT_LEAF_BIT) {
            root = make_node_ptr();
            loadTreeImpl(serealized, i, root->left);
            loadTreeImpl(serealized, i, root->right);
        }
        else {
            block_t value = 0;
            for (int j = 0; j < _blockLength; ++j) {
                value <<= 1;
                value |= serealized[i++] ? 1 : 0;
            }
            root = make_node_ptr(value);
        }
    }

    void calculateCodes() const {
        if (_updated) {
            _updated = false;

            _codes.clear();

            if (_root->leaf) {
                _codes.emplace(_root->value, bits{ 0 });
            }
            else {
                calculateCodesImpl(_root, bits());
            }
        }
    }

    void calculateCodesImpl(node_ptr tree, const bits& curPrefix) const {
        if (tree->leaf) {
            _codes.emplace(tree->value, curPrefix);
        }
        else {
            bits temp = curPrefix;
            temp.push_back(LEFT_BIT);
            calculateCodesImpl(tree->left, temp);
            temp.back() = RIGHT_BIT;
            calculateCodesImpl(tree->right, temp);
        }
    }

    node_ptr _root;
    int _blockLength;

    mutable bool _updated;
    mutable set<pair<block_t, bits>, comp> _codes;
};

bits encodeHuffman(const bits &original, int blockLen = 0) {
    if (!blockLen)
        blockLen = optimalBlockLength(original);

    bits sequence = original;

    padSequence(sequence, blockLen);

    HuffmanTree tree;
    tree.buildTree(blocksCount(sequence, blockLen), blockLen);

//    tree.printCodes();

    bits encoded;
    encoded.reserve(sequence.size());

    // область видимости сереализованного дерева
    {
        auto ser = tree.serealize();
        encoded.insert(encoded.end(), ser.begin(), ser.end());
    }

    block_t block = 0;
    int i = 0;

    for (auto b : sequence) {
        block <<= 1;
        block |= b ? 1 : 0;

        if (++i == blockLen) {
            bits blockseq = tree.encode(block);
            encoded.insert(encoded.end(), blockseq.begin(), blockseq.end());
            block = 0;
            i = 0;
        }
    }

    sequence.clear();

    encoded.reserve(encoded.size());

    return std::move(encoded);
}

bits decodeHuffman(const bits &encoded) {
    HuffmanTree tree;

    int i = tree.load(encoded);

    bits decoded;
    decoded.reserve(encoded .size() * 2);

    bits encodedBlock;
    while (i < encoded.size()) {
        encodedBlock.push_back(encoded[i++]);

        if (tree.containSequence(encodedBlock)) {
            block_t block = tree.decode(encodedBlock);
            bits bitblock;
            for (int j = tree.getBlockLength() - 1; j >= 0; --j)
                bitblock.push_back((block >> j) & 1);
            decoded.insert(decoded.end(), bitblock.begin(), bitblock.end());
            encodedBlock.clear();
        }
    }

    decoded.reserve(decoded.size());

    unpadSequence(decoded, tree.getBlockLength());

    return decoded;
}

void addPrefix(bits &seq, int prefix, int prefixlength) {
    bits prefBits(prefixlength);

    for (int i = 0; i < prefixlength; ++i) {
        prefBits[prefBits.size() - i - 1] = (prefix >> i) & 1;
    }

    prefBits.insert(prefBits.end(), seq.begin(), seq.end());

    seq = std::move(prefBits);
}

int removePrefix(bits &seq, int prefixLength) {
    int prefix = 0;

    for (int i = 0; i < prefixLength; ++i) {
        prefix <<= 1;
        prefix |= (seq[i] ? 1 : 0);
    }

    seq = std::move(bits(seq.begin() + prefixLength, seq.end()));

    return prefix;
}

bits adaptiveEncode(const bits &seq) {
    bits encoded1 = seq;
    bits encoded2;

    int count = -1;

    do {
        ++count;
        encoded2 = std::move(encoded1);
        encoded1 = std::move(encodeHuffman(encoded2));
    } while (encoded1.size() < encoded2.size() && count < MAX_CYCLE);

    addPrefix(encoded2, count, CYCLE_COUNT_LEN);

    return std::move(encoded2);
}

bits adaptiveDecode(const bits &seq) {
    bits decoded = seq;

    int count = removePrefix(decoded, CYCLE_COUNT_LEN);

    for (int i = 0; i < count; ++i) {
        decoded = std::move(decodeHuffman(decoded));
    }

    return std::move(decoded);
}

bits stringToBits(const string &str) {
    bits result;
    result.reserve(str.size() * sizeof(char));

    for (const auto &c : str) {
        for (int i = 0; i < 8 * sizeof(char); ++i) {
            result.push_back((c >> i) & 1);
        }
    }

    return result;
}

string bitsToString(const bits &seq) {
    if (seq.size() & 0x07 != 0) {
        throw "Incorrect sequence size";
    }

    string result;
    result.reserve(seq.size() / 8);

    char c = 0;
    for (int i = 0; i < seq.size(); ++i) {
        if (seq[i]) {
            c |= 1 << (i & 0x07);
        }
        if (((i + 1) & 0x07) == 0) {
            result.push_back(c);
            c = 0;
        }
    }

    return result;
}

// добавление каждого восьмого бита, чтобы не было нулевых байт
void addBits(bits &seq) {
    for (int i = 0; i < seq.size(); i += 8) {
        seq.insert(seq.begin() + i, true);
    }
}

// удаление добавленных бит
void removeBits(bits &seq) {
    for (int i = (seq.size() - 1 - ((seq.size() - 1) % 8)); i >= 0; i -= 8) {
        seq.erase(seq.begin() + i);
    }
}

string compress(const string &original) {
    auto seq = stringToBits(original);

    seq = adaptiveEncode(seq);
    addBits(seq);
    padSequence(seq, sizeof(char) * 8);

    return bitsToString(seq);
}

string decompress(const string &compressed) {
    auto seq = stringToBits(compressed);

    unpadSequence(seq, sizeof(char) * 8);
    removeBits(seq);
    seq = adaptiveDecode(seq);

    return bitsToString(seq);
}
