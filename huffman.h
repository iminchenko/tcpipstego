#pragma once

#include <string>

std::string compress(const std::string &original);
std::string decompress(const std::string &compressed);
