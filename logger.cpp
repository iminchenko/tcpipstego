#include <QThread>

#include "logger.h"

using std::string;

Logger &Logger::instance() {
    static Logger logger;
    return logger;
}

void Logger::addLogMessage(const QString &msg) {
    instance().addLogMessageImpl(msg);
}

void Logger::addSymbols(const string &symbs) {
    instance().addSymbolsImpl(symbs);
}

void Logger::addSymbols(char c) {
    instance()._buffer.push_back(c);
}

void Logger::messageRecieved() {
    instance().messageRecievedImpl();
}

void Logger::setLoggingFunction(std::function<void(std::string)> func) {
    _func = func;
}

Logger::Logger(QObject *parent) : QObject(parent) {}

void Logger::addLogMessageImpl(const QString &msg) {
    emit messageLogged(msg);
}

void Logger::addSymbolsImpl(const string &symbs) {
    _buffer += symbs;
}

void Logger::messageRecievedImpl() {
    if (_func) {
        _func(_buffer);
    }
    _buffer.clear();
}
