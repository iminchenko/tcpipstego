#pragma once

#include <QObject>
#include <QString>
#include <string>
#include <functional>

class Logger : public QObject {
    Q_OBJECT
public:
    static Logger &instance();
    static void addLogMessage(const QString &msg = QString());
    static void addSymbols(const std::string &symbs);
    static void addSymbols(char c);
    static void messageRecieved();

    void setLoggingFunction(std::function<void(std::string)> func);

signals:
    void messageLogged(QString);

public slots:

private:
    explicit Logger(QObject *parent = nullptr);
    void addLogMessageImpl(const QString &msg);
    void addSymbolsImpl(const std::string &symbs);
    void messageRecievedImpl();

    std::string _buffer;
    std::function<void(std::string)> _func;
};
