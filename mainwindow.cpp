#include <atomic>
#include <QString>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "tcpoperations.h"
#include "logger.h"
#include "huffman.h"

using std::string;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow), _isListening(false) {
    ui->setupUi(this);

    connect(&(Logger::instance()), SIGNAL(messageLogged(QString)),
            this, SLOT(addLineEntry(QString)));

    std::atomic<bool> test;
    test = false;

    Logger::instance().setLoggingFunction([&](std::string str) {
                                              this->on_message_recieved(str);
                                          });
}

MainWindow::~MainWindow() {
    delete ui;
    _isListening = false;
}

void MainWindow::addLineEntry(QString entry) {
    ui->listWidget->addItem(entry);
}

void MainWindow::on_message_recieved(std::string msg) {
    if (_isListening) {
        for (auto i : msg) {
            _entropyCalculator.addSymbol(i);
        }

        ui->label_entropy->setText(QString::number(_entropyCalculator.entropy()));
    }

    QString qstr = msg.c_str();
    try {
        if (ui->checkBox_compress->isChecked()) {
            qstr = QString(decompress(msg).c_str());
        }
    } catch (...) {
        qstr = QString(msg.c_str()) + " [catched]";
    }
    ui->listWidget->addItem(qstr);
}

void MainWindow::on_pushButton_send_clicked() {
    string message = ui->lineEdit_message->text().toStdString();

    if (ui->checkBox_compress->isChecked()) {
        message = compress(message);
    }

    sendMessage(message, ui->lineEdit_sourceIp->text().toStdString(),
                        ui->lineEdit_destIp->text().toStdString());

    ui->lineEdit_message->clear();
}

void MainWindow::on_pushButton_server_clicked() {
    if (_future.valid()) {
        _isListening = !_isListening;
        _future.wait();
        _isListening = !_isListening;
    }

    if (_isListening) {
        ui->pushButton_server->setText("Начать прослушивание");
    } else {
        ui->pushButton_server->setText("Остановить прослушивание");
        _future = std::async(listen,
                             ui->lineEdit_sourceIp->text().toStdString(),
                             ui->lineEdit_destIp->text().toStdString(),
                             std::ref(_isListening));
    }

    ui->lineEdit_destIp->setEnabled(_isListening);
    ui->lineEdit_sourceIp->setEnabled(_isListening);
    ui->lineEdit_message->setEnabled(_isListening);
    ui->pushButton_send->setEnabled(_isListening);
    ui->checkBox_compress->setEnabled(_isListening);
    ui->checkBox_encrypt->setEnabled(_isListening);
    ui->checkBox_link->setEnabled(_isListening);

    _isListening = !_isListening;
}

void MainWindow::on_pushButton_clear_clicked() {
    if (_isListening) {
        ui->pushButton_server->click();
    }
    _entropyCalculator.clear();

//    ui->lineEdit_destIp->clear();
//    ui->lineEdit_sourceIp->clear();
    ui->listWidget->clear();

    ui->lineEdit_message->clear();
    ui->checkBox_compress->setChecked(false);
    ui->checkBox_encrypt->setChecked(false);
    ui->checkBox_link->setChecked(false);

    ui->label_entropy->setText("0");
}
