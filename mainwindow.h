#pragma once

#include <QMainWindow>
#include <atomic>
#include <future>

#include "entropycalculator.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void on_message_recieved(std::string msg);

private slots:
    void addLineEntry(QString entry);

    void on_pushButton_send_clicked();

    void on_pushButton_server_clicked();

    void on_pushButton_clear_clicked();

private:
    Ui::MainWindow *ui;
    std::atomic<bool> _isListening;
    std::future<void> _future;

    EntropyCalculator _entropyCalculator;
};
