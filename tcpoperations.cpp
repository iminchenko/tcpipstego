#include <string.h>          //memset
#include <sys/socket.h>      //for socket ofcourse
#include <errno.h>           //For errno - the error number
#include <netinet/tcp.h>     //Provides declarations for tcp header
#include <netinet/ip.h>      //Provides declarations for ip header
#include <unistd.h>
#include <arpa/inet.h>
#include <QLatin1Char>

#include "tcpoperations.h"
#include "logger.h"

using std::string;

// 96 бит (12 байт) псевдо-заголовок, нужный для вычисления хэш-суммы
struct pseudo_header {
    u_int32_t source_address;
    u_int32_t dest_address;
    u_int8_t placeholder;
    u_int8_t protocol;
    u_int16_t tcp_length;
};

// функция вычисление хэш-суммы
unsigned short csum(unsigned short *ptr, int nbytes) {
    long sum;

    sum=0;
    while (nbytes > 1) {
        sum += *ptr++;
        nbytes -= 2;
    }
    if (nbytes == 1) {
        sum += *ptr;
    }

    sum = (sum >> 16)+(sum & 0xffff);
    sum = sum + (sum >> 16);

    return (short)~sum;
}


void sendMessage(const std::string &msg, const std::string &sourceIp,
                 const std::string &destIp) {
    // ничего не происходит при отсутствии сообщения
    if (!msg.size()) {
        Logger::instance().addLogMessage("Message is empty");
        return;
    }

    for (auto i : msg) {
        Logger::addSymbols((QString::number(i) + " ").toStdString());
    }
    Logger::messageRecieved();

    Logger::addLogMessage("Sending message \"" + QString(msg.c_str()) + "\"");

    // вычисление необходимого количества пакетов
    int n = (msg.size() + 6) / 6;

    int i;
    for (i = 0; i < n; ++i) {
        // задержка между отправлением пакетов
        usleep(10000);
        // Создание сокета в RAW режиме
        int s = socket (PF_INET, SOCK_RAW, IPPROTO_TCP);

        if(s == -1) {
            // Создание сокета закончилось ошибкой
            // вероятно, из-за отсутствия привелегий
            Logger::addLogMessage("Failed to create socket");
            return;
        }

        // побитовове представление пакета
        char datagram[4096], *pseudogram;

        // инициалиация нулями
        memset(datagram, 0, 4096);

        // IP заголовок
        struct iphdr *iph = (struct iphdr *) datagram;

        //TCP заголовок
        struct tcphdr *tcph = (struct tcphdr *) (datagram + sizeof(struct ip));
        struct sockaddr_in sin;
        struct pseudo_header psh;

        //some address resolution
        sin.sin_family = AF_INET;
        sin.sin_port = htons(80);
        sin.sin_addr.s_addr = inet_addr(destIp.c_str());

        // Заполнение IP заоловка
        // Минимальная корректная длина 5
        iph->ihl = 5;
        // IPv3
        iph->version = 4;
        // приоритет не важен
        iph->tos = 0;
        // вычисление общей длины пакета
        iph->tot_len = sizeof (struct iphdr) + sizeof (struct tcphdr);
        // первая часть полезной нагрузки
        iph->id = 0;
        if (6 * i < msg.size()) {
            iph->id |= int(msg[6*i]) << 8;
        }
        if (6 * i + 1 < msg.size()) {
            iph->id |= (unsigned char)msg[6*i + 1];
        }
        // Если id == 0, он заменяеся на случайное значение
        // При присвоении ему единицы, первая половина останется нулевой,
        // что соответствует концу строки
        if (iph->id == 0)
            iph->id = 1;
        // Первый фрагмент => нулевое смещение
        iph->frag_off = 0;
        // Стандартный во многих случаях TTL
        iph->ttl = 64;
        // Протокол TCP
        iph->protocol = IPPROTO_TCP;
        // Выставление нуля перед вычислением хэш-суммы
        iph->check = 0;
        // Исходящий IP
        iph->saddr = inet_addr (sourceIp.c_str());
        // IP точки назначения
        iph->daddr = sin.sin_addr.s_addr;

        // Вычисление хэша IP заголовка
        iph->check = csum ((unsigned short *) datagram, iph->tot_len);

        // Заголовок TCP
        // порт источника
        tcph->source = htons (20);
        // порт цели
        tcph->dest = htons (rand() % 10000); // "сканируем" разные порты
        tcph->ack_seq = 0;

        // Вторая часть полезной нагрузки
        tcph->seq = 0;
        unsigned char data = 0;
        int j;
        for (j = 0; j < 4; ++j) {
            if (6*i + 2 + j < msg.size()) {
                data = msg[6*i + 2 + j];
                tcph->seq |= data << (8 * j);
            } else {
                tcph->seq &= ~(255 << 8 * j);
            }
        }

        // сдвиг равен размеру заголовка
        tcph->doff = 5;
        // Из флагов интересует только SYN
        tcph->fin=0;
        tcph->syn=1;
        tcph->rst=0;
        tcph->psh=0;
        tcph->ack=0;
        tcph->urg=0;
        // максимальный размер окна
        tcph->window = htons(5840);
        // хэш-сумма будет заполнена при помощи псевдо-заголовка
        tcph->check = 0;
        // нулевая "важность"
        tcph->urg_ptr = 0;

        // Подготовка к вычислению хэша
        psh.source_address = inet_addr(sourceIp.c_str());
        psh.dest_address = sin.sin_addr.s_addr;
        psh.placeholder = 0;
        psh.protocol = IPPROTO_TCP;
        psh.tcp_length = 0;

        int psize = sizeof(struct pseudo_header) + sizeof(struct tcphdr);
        pseudogram = (char*)malloc(psize);

        memcpy(pseudogram , (char *) &psh , sizeof (struct pseudo_header));
        memcpy(pseudogram + sizeof(struct pseudo_header) , tcph,
               sizeof(struct tcphdr));
        tcph->check = csum((unsigned short *) pseudogram , psize);

        free(pseudogram);


        // IP_HDRINCL чтобы сказать ядру, что заголовки включены в пакет
        int one = 1;
        const int *val = &one;

        if (setsockopt (s, IPPROTO_IP, IP_HDRINCL, val, sizeof (one)) < 0) {
            Logger::addLogMessage("Error setting IP_HDRINCL");
            return;
        }
        // Отправка пакета
        if (sendto(s, datagram, iph->tot_len, 0, (struct sockaddr *) &sin, sizeof (sin)) < 0) {
            Logger::addLogMessage("sendto failed");
            return;
        }
        // Успех
        else {
            QString temp;
            for (int j = 0; j < 6; ++j) {
                temp.append(msg[6 * i + j]);
            }
            // информация об отправленном пакете

            Logger::addLogMessage("Packet sent. \"" + temp + "\"");

            Logger::addSymbols(QString::number(char((iph->id >> 8) & 255)).toStdString());
            Logger::addSymbols(" ");
            Logger::addSymbols(QString::number(char((iph->id) & 255)).toStdString());
            Logger::addSymbols(" ");

            for (int i = 0; i < 4; ++i) {
                Logger::addSymbols(QString::number(char((tcph->seq >> 8 * i) & 255)).toStdString());
                Logger::addSymbols(" ");
            }
            Logger::messageRecieved();
        }
    }

    return;
}

int sock_raw;
// буфер для хранения сообщения
char global_buffer[1024];
// текущий размер сообщения
int global_n = 0;
std::string src_addr, dst_addr;

void handleMessage(unsigned char *Buffer) {
    int i;

    struct iphdr *iph = (struct iphdr *)Buffer;
    struct tcphdr *tcph = (struct tcphdr *) (Buffer + sizeof (struct ip));

    struct sockaddr_in source,dest;
    // получение адресов цели и источника
    memset(&source, 0, sizeof(source));
    source.sin_addr.s_addr = iph->saddr;

    memset(&dest, 0, sizeof(dest));
    dest.sin_addr.s_addr = iph->daddr;
    // если адрес источника совпадает с требуемым
    if (source.sin_addr.s_addr == inet_addr(src_addr.c_str())
            && dest.sin_addr.s_addr == inet_addr(dst_addr.c_str())) {
        // выбор потока вывода
        setvbuf (stdout, NULL, _IONBF, 0);
        // массив с полезной нагрузкой
        char payload[6];
        // извлечение первой часли полезной нагрузки
        payload[0] = iph->id >> 8;
        payload[1] = iph->id & ((1 << 8) - 1);
        //извлечение второй части полезной нагрузки
        for (i = 0; i < 4; ++i) {
            payload[i + 2] = (tcph->seq >> i*8) & ((1 << 8 ) - 1);
        }
        // копирование глобальный буфер
        for (i = 0; i < 6; ++i) {
            global_buffer[global_n++] = payload[i];
        }
        // вывод полученной части сообщения
        for (i = 0; i < 6; ++i) {
            // проверка на окончание
            if (payload[i]) {
                Logger::addSymbols(payload[i]);
                printf("%d ", payload[i]);
            } else {
                // 0 => конец сообщения
                puts("");
                Logger::messageRecieved();
                // сброс текущей длины сообщения
                global_n = 0;
                // выход из цикла
                break;
            }
        }
    }
}

void processPacket(unsigned char* buffer) {
    // Получение указателя на IP заголовок пакета
    struct iphdr *iph = (struct iphdr*)buffer;

    // Если получен TCP пакет
    if (iph->protocol == IPPROTO_TCP) {
        // обработка сообщения
        handleMessage(buffer);
    }
}

void listen(const std::string &sourceIp, const std::string &destIp,
            const std::atomic<bool> &isListening) {
    int saddr_size , data_size;
    struct sockaddr saddr;

    src_addr = sourceIp;
    dst_addr = destIp;

    unsigned char *buffer = (unsigned char *)malloc(65536);

    Logger::addLogMessage("Starting...");
    // Создание "сырого" сокета, который будет прослушивать
    sock_raw = socket(AF_INET , SOCK_RAW , IPPROTO_TCP);
    if(sock_raw < 0)     {
        Logger::addLogMessage("Socket Error\n");
    }

    while(isListening) {
        saddr_size = sizeof(saddr);
        // Получение пакета
        data_size = recvfrom(sock_raw , buffer , 65536 , 0 , &saddr , (socklen_t *)&saddr_size);
        if(data_size < 0) {
            Logger::addLogMessage("Recvfrom error , failed to get packets\n");
            break;
        }
        // Обработка пакета
        processPacket(buffer);
    }
    close(sock_raw);
    Logger::addLogMessage("Finished");
}
