#pragma once

#include <string>
#include <atomic>

void sendMessage(const std::string& msg, const std::string &sourceIp,
                 const std::string &destIp);

void listen(const std::string &sourceIp, const std::string &destIp,
            const std::atomic<bool> &isListening);
